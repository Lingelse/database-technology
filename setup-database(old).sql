-- CREATE DATABASE mydb;

\connect mydb;
SET client_encoding TO 'UTF8';


\echo 'Create bands table'

DROP TABLE IF EXISTS bands CASCADE;

CREATE TABLE bands (
    band_src varchar(260) PRIMARY KEY,
    band_name varchar(260) NOT NULL UNIQUE
);

\copy bands FROM 'band-band_name.csv' WITH (FORMAT csv, DELIMITER ',');


\echo 'Create members table'

DROP TABLE IF EXISTS members;

CREATE TABLE members (
    band_src varchar(260) NOT NULL,
    member_src varchar(260) NOT NULL,
    member_name varchar(260) NOT NULL,
    FOREIGN KEY (band_src) REFERENCES bands(band_src) ON DELETE CASCADE,
    PRIMARY KEY (band_src,member_name)
);

\copy members FROM 'C:\Users\leoni\Desktop\Master\Database Technologies\Project 1\src\band-member-member_name.csv' WITH (FORMAT csv, DELIMITER ',');


\echo 'Create former members table'

DROP TABLE IF EXISTS former_members;

CREATE TABLE former_members (
    band_src varchar(260) NOT NULL,
    member_src varchar(260) NOT NULL,
    member_name varchar(260) NOT NULL,
    FOREIGN KEY (band_src) REFERENCES bands(band_src) ON DELETE CASCADE,
    PRIMARY KEY (band_src,member_name)
);

\copy former_members FROM 'band-former_member-member_name.csv' WITH (FORMAT csv, DELIMITER ',');


\echo 'Create genres table'

DROP TABLE IF EXISTS genres;

CREATE TABLE genres (
    band_src varchar(260) NOT NULL,
    genre varchar(260) NOT NULL,
    FOREIGN KEY (band_src) REFERENCES bands(band_src) ON DELETE CASCADE,
    PRIMARY KEY (band_src,genre)
);

\copy genres FROM 'band-genre_name.csv' WITH (FORMAT csv, DELIMITER ',');



\echo 'Create albums table'

DROP TABLE IF EXISTS albums;

CREATE TABLE albums (
    band_src varchar(260) NOT NULL,
    album_name varchar(260) NOT NULL,
    release_data DATE,
    album_description varchar,
    album_value float,
    album_value_2 float,
    FOREIGN KEY (band_src) REFERENCES bands(band_src) ON DELETE CASCADE,
    PRIMARY KEY (band_src,album_name,release_data,album_value,album_value_2)
);

\copy albums FROM 'band-album_data.csv' WITH (FORMAT csv, DELIMITER ',');


