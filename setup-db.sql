-- CREATE DATABASE bands;

\connect bands;
SET client_encoding TO 'UTF8';

DROP TABLE IF EXISTS bands CASCADE;

CREATE TABLE bands (
    band_id int PRIMARY KEY, 
    band_name varchar(100) NOT NULL);

\copy bands FROM 'bands.csv' WITH (FORMAT csv, DELIMITER ',');


------------------------------------------------------------------------
DROP TABLE IF EXISTS musicians CASCADE;

CREATE TABLE musicians (
    musician_id int PRIMARY KEY);

\copy musicians FROM 'musician_ids.csv' WITH (FORMAT csv, DELIMITER ',');


------------------------------------------------------------------------
DROP TABLE IF EXISTS artist_names CASCADE;

CREATE TABLE temp_artist_names ( 
    performs_in_id int PRIMARY KEY,
    band_id int NOT NULL,
    musician_id int NOT NULL, 
    name varchar(100) NOT NULL,
    currently_playing BOOLEAN
);

\copy temp_artist_names FROM 'Artist names.csv' WITH (FORMAT csv, DELIMITER ',');

CREATE TABLE artist_names ( 
    musician_id int NOT NULL, 
    name varchar(100) NOT NULL, 
    PRIMARY KEY (name,musician_id), 
    FOREIGN KEY (musician_id) REFERENCES musicians ON DELETE CASCADE);

INSERT INTO artist_names
    SELECT DISTINCT musician_id,name
    FROM temp_artist_names;


------------------------------------------------------------------------
DROP TABLE IF EXISTS performs_in CASCADE;

CREATE TABLE performs_in (
    performs_in_id int PRIMARY KEY,
    band_id int,
    musician_id int,
    currently BOOLEAN,
    FOREIGN KEY (musician_id) REFERENCES musicians ON DELETE CASCADE,
    FOREIGN KEY (band_id) REFERENCES bands ON DELETE CASCADE
);

INSERT INTO performs_in
    SELECT DISTINCT performs_in_id,band_id,musician_id,currently_playing
    FROM temp_artist_names;

DROP TABLE IF EXISTS temp_artist_names CASCADE;


------------------------------------------------------------------------
DROP TABLE IF EXISTS band_genre_association CASCADE;

CREATE TABLE band_genre_association (
    band_id int,
    genre varchar(50),
    FOREIGN KEY (band_id) REFERENCES bands,
    PRIMARY KEY (band_id,genre)
);

\copy band_genre_association FROM 'band genre association.csv' WITH (FORMAT csv, DELIMITER ',');


------------------------------------------------------------------------
DROP TABLE IF EXISTS albums CASCADE;

CREATE TABLE temp_albums (
    version_id int PRIMARY KEY,
    album_name varchar(150) NOT NULL, 
    album_id int, 
    band_id int,
    date DATE, 
    abstract varchar(10000),
    running_time float,
    sales float 
);

\copy temp_albums FROM 'albums.csv' WITH (FORMAT csv, DELIMITER ',');


CREATE TABLE albums (
    album_id int PRIMARY KEY, 
    band_id int,
    album_name varchar(150) NOT NULL, 
    abstract varchar(10000),
    FOREIGN KEY (band_id) REFERENCES bands
);

INSERT INTO albums
    SELECT DISTINCT album_id, band_id, album_name, abstract
    FROM temp_albums;

------------------------------------------------------------------------
DROP TABLE IF EXISTS versions CASCADE;

CREATE TABLE versions (
    version_id int PRIMARY KEY, 
    album_id int,
    running_time float,
    date DATE, 
    sales float, 
    FOREIGN KEY (album_id) REFERENCES albums
);

INSERT INTO versions
    SELECT DISTINCT version_id,album_id,running_time,date,sales
    FROM temp_albums;

DROP TABLE IF EXISTS temp_albums CASCADE;

